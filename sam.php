<?php
/**
 * Plugin Name: Sam
 * Plugin URI: http://www.sam.gaaf.io/samlogo
 * Description: De wonderbaarlijke plugin van Sam Harke.
 * Version: 3.0
 * Author: Sam Harke
 * Author URI: http://www.sam.gaaf.io/
 * License: GPL2
 */
add_action( 'the_content', 'my_thank_you_text' );

function my_thank_you_text ( $content ) {
    return $content .= '<p>Deze tekst wordt gegenereerd door mijn plugin; Sam.</p>';
}

require 'plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/SamHarke/pluginsam/',
	__FILE__,
	'Nieuwe update 3.0'
);